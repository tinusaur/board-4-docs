# Tinusaur Main Board Docs

Tinusaur Main Board Documentation

## Tinusaur Main Board Assembling Guide

[![Tinusaur Main Board Assembling Guide](Tinusaur_Board_Gen4_mkII_Assembling_Guide.jpg)](https://gitlab.com/tinusaur/board-4-docs/-/raw/master/Tinusaur_Board_Gen4_mkII_Assembling_Guide.pdf?ref_type=heads&inline=false)


- Download link: https://gitlab.com/tinusaur/board-4-docs/-/raw/master/Tinusaur_Board_Gen4_mkII_Assembling_Guide.pdf?inline=false

